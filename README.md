# Backend

Author: Mateus Jorge <br />

[Linkedln profile](https://www.linkedin.com/in/mateusjorge28186/) <br />
[GitHub profile](https://github.com/mavismmg) <br />
[Kaggle profile](https://www.kaggle.com/mateusjorge) <br />   

This repository contains the sprints from 1 to 5 to 'Processo Seletivo 22.1' <br />

[Sprint 1 - Processo seletivo TerraLab 2022/2](https://gitlab.com/processo-seletivo-terralab1/backend/-/tree/sprint1) <br />

[Sprint 2 - Processo seletivo TerraLab 2022/2](https://gitlab.com/processo-seletivo-terralab1/backend/-/tree/sprint2) <br />
